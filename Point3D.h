//
// Created by restil on 23/11/17.
//

#ifndef INFOGRAPHIECLASSE_POINT3D_H
#define INFOGRAPHIECLASSE_POINT3D_H


class Point3D {
public:
    float x, y, z;
    void set(float x, float y, float z);
    Point3D(float x, float y, float z);

    Point3D();
};


#endif //INFOGRAPHIECLASSE_POINT3D_H
