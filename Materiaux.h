//
// Created by restil on 23/11/17.
//

#ifndef INFOGRAPHIECLASSE_MATERIAUX_H
#define INFOGRAPHIECLASSE_MATERIAUX_H


#include <GL/gl.h>
#include "Point3D.h"
#include <list>

using namespace std;

class Couleur {
public:
    Couleur();
    Couleur(Couleur const& autre);
    Couleur(float r, float g, float b);
    Couleur(float r, float g, float b, float a);
   // ~Couleur();
    float r;
    float g;
    float b;
    float a=1.;
};

class Image{
public:
    Image(char* adresseImage);
    //Image(GLuint autre);

    Image(const Image &autre);

    GLuint getImage();
    char* adresse;
    GLuint image;
    bool a = false;
};
class Texture {
public:
    Couleur* couleur = nullptr;
    Image* image = nullptr;

    Texture();
    Texture(Texture const& autre);
    Texture(const Couleur &couleur);
    Texture(char* image);
    Texture(const Couleur &couleur, char* image);
    ~Texture();
    bool isCouleur();
    bool isTexture();
    bool isAll();
};

class Materiaux {
public:
    Materiaux();
    Materiaux(Materiaux const& autre);
    Materiaux(Texture tabTexture[]);
    Materiaux(const Couleur &couleur);
    Materiaux(Texture texture);
    ~Materiaux();
    GLuint getObjet();
    void setSpeculaire(Couleur a);

private:
    GLuint objet= 0;
    float mat_ambiante[4] = {1.0,1.0,1.0,1.0};
    float mat_diffuse[4] = {1.0,1.0,1.0,1.0};
    float mat_speculaire[4] = {0.1,0.1,0.1,0.0};
    Texture textures[6];
    void activeTexture(Texture texture);
    void init();
};


#endif //INFOGRAPHIECLASSE_MATERIAUX_H
