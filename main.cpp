#include <iostream>

#include <math.h>
#include "GL/glut.h"
#include <stdio.h>
#include <stdlib.h>
//#include "tga.h"
#include "Objet.h"
#include "Materiaux.h"
#include "Point3D.h"
#include <list>

#define D_ANGLE 0.015708
#define FPS 30

#define RIEN 0
#define GAUCHE 1
#define DROIT 2
#define MILIEU 3

using namespace std;
GLuint ancien_x, ancien_y, mouv;
GLdouble angle = -.0, tetha = 0., hauteur = 2.35, distanceUser = 3., rayon = 2.;
list<Objet> scene;
GLfloat Cx = 3.3, Cy=1.,Cz=4.5;


void initObjet(){
    /*
     *
     * Differentes Mesures de la Salle A106
     *
     * */

    /* Différentes donnees non mesure maix fixee par moi*/

    double epaisseurMur = 0.2;

    /* Dimension Salle */
    double salleX = 6.7;
    double salleY = 2.73;
    double salleZ = 9.63;
    /* Dimension Porte */
    double porteX = 0.985;
    double porteY = 1.824;
    double porteZ = 0.038;
    /* Avant porte murDevant */
    double avPorteX = 0.14;
    double avPorteZ = 0.14;
    /* Poignee */
    double p1X = 0.02;
    double p1Y = 0.02;
    double p1Z = 0.043;
    double p2X = 0.135;
    double p2Y = 0.02;
    double p2Z = 0.02;
    double p3X = 0.05;
    double p3Y = 0.05;
    double p3Z = 0.01;
    double placeX = 2*porteX/3;
    double placeY = porteY/2;
    double placez = porteZ;
    /* Clavier */
    double clavierX = 0.45;
    double clavierY = 0.025;
    double clavierZ = 0.14;
    /* Ecran PC */
    double socleX = 0.18;
    double socleY = 0.02;
    double socleZ = 0.16;
/*
 * float xBoutV=0.07;
	float yBoutV=0.08;
	float zBoutV=0.02;

	float xBoutH=0.07;
	float yBoutH=0.02;
	float zBoutH=0.055;
 */
    double cadreHX = 0.02;
    double cadreHY = 0.27;
    double cadreHZ = 0.04;
    double cadreLX = 0.43;
    double cadreLY = 0.02;
    double cadreLZ = 0.04;


    /* Creation Materiaux */
    Texture tabSol[] = {Texture(Couleur(0.686,0.671,0.671)), Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(0,0,0)),Texture("sol.tga")};
    Materiaux mSol = Materiaux(tabSol);
    Texture tabPlafond[] = {Texture(Couleur(0.686,0.671,0.671)), Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(0.686,0.671,0.671)),Texture(Couleur(1,1,1),"plafond.tga"), Texture(Couleur(1,1,1))};
    Materiaux mPlafond = Materiaux(tabPlafond);
    //Materiaux mMur = Materiaux(Couleur(0.686,0.671,0.671));
    Materiaux mMur = Materiaux(Couleur(0.933, 0.910, 0.667));
    mMur.setSpeculaire(Couleur(0,0,0,0));
    Materiaux mPorte = Materiaux(Couleur(0.,0.66,0.));
    Materiaux mPoignee = Materiaux(Couleur(1,1,1));
    Texture tabClavier[] = {Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture("clavier.tga")};
    Materiaux mClavier = Materiaux(tabClavier);
    Materiaux mCadre = Materiaux(Couleur(0,0,0));
    Materiaux mVitre = Materiaux(Couleur(0.6,1,1,0.2));
    Materiaux mAcierContourVitre = Materiaux(Couleur(0,0,0));
    Materiaux mPlastiqueEcran = Materiaux(Couleur(0,0,0));
    Materiaux mEcran = Materiaux("ecran.tga");
    Texture tabBois[] = {Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0.6,0.6,1),"dessusTable.tga")};
    Materiaux mBois = Materiaux(tabBois);
    Texture tabPC[] = {Texture("tourArriere.tga"), Texture("tourAvant.tga"), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0))};
    Materiaux mPC = Materiaux(tabPC);
    Materiaux mPiedsTable = Materiaux(Couleur(0,0,0));
    Texture tabExterieur1[] = {Texture(Couleur(1,1,1)), Texture("decorD.tga"), Texture(Couleur(1,1,1)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0))};
    Materiaux mExterieur1 = Materiaux(tabExterieur1);
    Texture tabExterieur2[] = {Texture(Couleur(1,1,1)),  Texture(Couleur(1,1,1)), Texture(Couleur(0,0,0)),Texture("decorF.tga"), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0))};
    Texture tabExterieur3[] = {Texture("decorG.tga"), Texture(Couleur(1,1,1)), Texture(Couleur(1,1,1)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0))};
    Materiaux mExterieur2 = Materiaux(tabExterieur2);
    Materiaux mExterieur3 = Materiaux(tabExterieur3);
    Texture tabSouris[] = {Texture("souris.tga"), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0)), Texture(Couleur(0,0,0))};
    Materiaux mSouris = Materiaux(tabSouris);
    Materiaux mChaise = Materiaux(Texture("chaise.tga"));
    Materiaux mPiedsChaise = Materiaux(Couleur(1,0.77,0));
    Texture tabTableau[] = {Texture(Couleur(0.,0.35,0.)), Texture("tableau.tga"), Texture(Couleur(0.,0.35,0.)), Texture(Couleur(0,0.35,0)), Texture(Couleur(0,0.35,0)), Texture(Couleur(0,0.35,0))};
    Materiaux mTableau = Materiaux(tabTableau);

    //Objet oSol = Objet(mSol,Point3D(salleX+2*epaisseurMur,epaisseurMur,salleZ+2*epaisseurMur),Point3D(0,0,0));
    // Objet oSol = Objet(mSol,Point3D(salleX+2*epaisseurMur,epaisseurMur,salleZ+2*epaisseurMur),Point3D(-epaisseurMur,-epaisseurMur,-epaisseurMur));
    Objet oSol = Objet(mSol, Point3D(salleX, epaisseurMur,salleZ), Point3D(0,-epaisseurMur,0));
    scene.push_back(oSol);

    Objet oPlafond = Objet(mPlafond, Point3D(salleX, epaisseurMur,salleZ), Point3D(0,salleY,0));
    scene.push_back(oPlafond);

    // Creation Porte
    list<Objet> listePorte;
    Objet oPorte1 = Objet(mPorte, Point3D(porteX,porteY,porteZ), Point3D(0,0,0));
    listePorte.push_back(oPorte1);
    // Creation poignee
    list<Objet> listeMorceauPoignee;
    Objet oPoignee1 = Objet(mPoignee, Point3D(p1X,p1Y,p1Z),Point3D(0,0,0));
    listeMorceauPoignee.push_back(oPoignee1);
    Objet oPoignee2 = Objet(mPoignee,Point3D(p2X,p2Y,p2Z),Point3D(-p2X+p1X,0,p1Z));
    listeMorceauPoignee.push_back(oPoignee2);
    Objet oPoignee3 = Objet(mPoignee,Point3D(p3X,p3Y,p3Z),Point3D(-p2X+p1X,0,p1Z-p3Z));
    listeMorceauPoignee.push_back(oPoignee3);
    Objet oPoignee = Objet(listeMorceauPoignee,Point3D(1,1,1),Point3D(placeX,placeY,placez));
    listePorte.push_back(oPoignee);
    Objet oPorte = Objet(listePorte,Point3D(1,1,1),Point3D(0,0,0));
    Objet oPorte2 = Objet(oPorte);
    Objet oPorte3 = Objet(oPorte);
    oPorte2.addRotation(1);
    oPorte3.addRotation(2);

    /* Fenetre */
    // Creation clavier
    Objet oClavier =  Objet(mClavier, Point3D(clavierX,clavierY,clavierZ), Point3D(0,0,0));
    // Creation ecran

    list<Objet> listeEcran;
    list<Point3D> listePlace; listePlace.push_back(Point3D(0,0,0));listePlace.push_back(Point3D(cadreHX+cadreLX,0,0));
    Objet oCadreH = Objet(mCadre, Point3D(cadreHX,cadreHY,cadreHZ),listePlace);
    list<Point3D> listePlace1; listePlace1.push_back(Point3D(cadreHX,0,0));listePlace1.push_back(Point3D(cadreHX,cadreHY-cadreLY,0));
    Objet oCadreL = Objet(mCadre,Point3D(cadreLX,cadreLY,cadreLZ), listePlace1);

    // Creations Mur Dev
    list<Objet> listeMorceauMurDev;
    Objet oMurDev1 = Objet(mMur, Point3D(epaisseurMur+avPorteX,salleY+2*epaisseurMur,epaisseurMur), Point3D(-epaisseurMur,-epaisseurMur,-epaisseurMur));
    listeMorceauMurDev.push_back(oMurDev1);
    Objet oMurDev2 = Objet(mMur, Point3D(porteX,+epaisseurMur+salleY-porteY,epaisseurMur), Point3D(avPorteX,porteY,-epaisseurMur));
    listeMorceauMurDev.push_back(oMurDev2);
    Objet oMurDev3 = Objet(mMur, Point3D(porteX,epaisseurMur,epaisseurMur), Point3D(avPorteX,-epaisseurMur, -epaisseurMur));
    listeMorceauMurDev.push_back(oMurDev3);
    Objet oMurDev4 = Objet(mMur, Point3D(salleX-porteX-avPorteX+epaisseurMur, salleY+2*epaisseurMur, epaisseurMur), Point3D(porteX+avPorteX, -epaisseurMur, -epaisseurMur));
    listeMorceauMurDev.push_back(oMurDev4);
    Objet oPorteMurDev = Objet(oPorte, Point3D(1,1,1), Point3D(avPorteX,0,0));
    listeMorceauMurDev.push_back(oPorteMurDev);
    listeMorceauMurDev.push_back(Objet(mTableau, Point3D(3.98,1.25,0.02), Point3D(2,0.82,0)));
    Objet oMurDev = Objet(listeMorceauMurDev, Point3D(1,1,1), Point3D(0,0,0));
    scene.push_back(oMurDev);

    // Creation Mur droite
    list<Objet> listeMorceauMurDroite;
    Objet oMurDr1 = Objet(mMur, Point3D(epaisseurMur,salleY+2*epaisseurMur, 1), Point3D(0,-epaisseurMur,0));
    listeMorceauMurDroite.push_back(oMurDr1);
    Objet oMurDr2 = Objet(mMur, Point3D(epaisseurMur,epaisseurMur,porteX), Point3D(0,-epaisseurMur,1));
    listeMorceauMurDroite.push_back(oMurDr1);
    Objet oPorteMurDr = Objet(oPorte2, Point3D(1,1,1), Point3D(0,0,1));
    listeMorceauMurDroite.push_back(oPorteMurDr);
    Objet oMurDr3 = Objet(mMur, Point3D(epaisseurMur,epaisseurMur+salleY-porteY, porteX), Point3D(0,porteY,1));
    listeMorceauMurDroite.push_back(oMurDr3);
    Objet oMurDr4 = Objet(mMur, Point3D(epaisseurMur,salleY+2*epaisseurMur,salleZ-1-porteX), Point3D(0,-epaisseurMur,1+porteX));
    listeMorceauMurDroite.push_back(oMurDr4);
    Objet oMurDroit = Objet(listeMorceauMurDroite, Point3D(1,1,1), Point3D(salleX,0,0));
    scene.push_back(oMurDroit);

    // Creations Mur Der
    list<Objet> listeMorceauMurDer;
    Objet oMurDer1 = Objet(mMur, Point3D(epaisseurMur+avPorteX,salleY+2*epaisseurMur,epaisseurMur), Point3D(-epaisseurMur,-epaisseurMur,0));
    listeMorceauMurDer.push_back(oMurDer1);
    Objet oMurDer2 = Objet(mMur, Point3D(porteX,+epaisseurMur+salleY-porteY,epaisseurMur), Point3D(avPorteX,porteY,0));
    listeMorceauMurDer.push_back(oMurDer2);
    Objet oMurDer3 = Objet(mMur, Point3D(porteX,epaisseurMur,epaisseurMur), Point3D(avPorteX,-epaisseurMur, 0));
    listeMorceauMurDer.push_back(oMurDer3);
    Objet oMurDer4 = Objet(mMur, Point3D(salleX-porteX-avPorteX+epaisseurMur, salleY+2*epaisseurMur, epaisseurMur), Point3D(porteX+avPorteX, -epaisseurMur, 0));
    listeMorceauMurDer.push_back(oMurDer4);
    Objet oPorteMurDer = Objet(oPorte3, Point3D(1,1,1), Point3D(avPorteX+porteX,0,0));
    listeMorceauMurDer.push_back(oPorteMurDer);
    Objet oMurDer = Objet(listeMorceauMurDer, Point3D(1,1,1), Point3D(0,0,salleZ));
    scene.push_back(oMurDer);

    /*
     *  Creation bloc fenêtre
     */
    list<Objet> listeMorceauBlocFenetre;
    list<Point3D> listePlaceContour1; listePlaceContour1.push_back(Point3D(0,0,0)); listePlaceContour1.push_back(Point3D(0,1.637,0));
    Objet oContour1 = Objet(mAcierContourVitre, Point3D(0.02,0.052,2.15),listePlaceContour1);
    listeMorceauBlocFenetre.push_back(oContour1);
    list<Point3D> listePlaceContour2; listePlaceContour2.push_back(Point3D(0,0.052,0)); listePlaceContour2.push_back(Point3D(0,0.052,2.088));
    Objet oContour2 = Objet(mAcierContourVitre, Point3D(0.02,1.585,0.062),listePlaceContour2);
    listeMorceauBlocFenetre.push_back(oContour2);
    Objet oContour3 = Objet(mAcierContourVitre,Point3D(0.02,0.09,2.026),Point3D(0,1.152,0.062));
    listeMorceauBlocFenetre.push_back(oContour3);
    Objet oContour4 =  Objet(mAcierContourVitre,Point3D(0.02,1.1,0.095),Point3D(0,0.052,0.582));
    listeMorceauBlocFenetre.push_back(oContour4);
    Objet oContour5 = Objet(mAcierContourVitre,Point3D(0.02,0.395,0.117),Point3D(0,1.242,1.473));
    listeMorceauBlocFenetre.push_back(oContour5);
    // Creation fenetres
    Objet oVitre1 = Objet(mVitre, Point3D(0.02,0.395,0.52), Point3D(0,1.242,1.568));
    listeMorceauBlocFenetre.push_back(oVitre1);
    Objet oVitre2 = Objet(mVitre, Point3D(0.02,0.395,1.389), Point3D(0,1.242,0.062));
    listeMorceauBlocFenetre.push_back(oVitre2);
    Objet oVitre3 = Objet(mVitre, Point3D(0.02,1.1,0.52), Point3D(0,0.052,0.062));
    listeMorceauBlocFenetre.push_back(oVitre3);
    Objet oVitre4 = Objet(mVitre, Point3D(0.02,1.1,1.411), Point3D(0,0.052,0.677));
    listeMorceauBlocFenetre.push_back(oVitre4);
    Objet oBlocFenetre = Objet(listeMorceauBlocFenetre,Point3D(1,1,1), Point3D(0,0,0));

    /*
     * Creation Ecran PC
     */
    list<Objet> listeMorceauEcran;
    list<Objet> listeBaseEcran;
    Objet oBaseEcran1 = Objet(mPlastiqueEcran, Point3D(0.18,0.02,0.16),Point3D(0,0,0));
    listeBaseEcran.push_back(oBaseEcran1);
    Objet oBaseEcran2 = Objet(mPlastiqueEcran, Point3D(0.07,0.08,0.02), Point3D(0.055,0.02,0.04));
    listeBaseEcran.push_back(oBaseEcran2);
    Objet oBaseEcran3 = Objet(mPlastiqueEcran, Point3D(0.07,0.02,0.055), Point3D(0.055,0.1,0.04));
    listeBaseEcran.push_back(oBaseEcran3);
    Objet oBaseEcran(listeBaseEcran, Point3D(1,1,1), Point3D(0,0,0));
    listeMorceauEcran.push_back(oBaseEcran);

    list<Objet> listeHautEcran;
    list<Point3D> listePlaceCadreEcran1; listePlaceCadreEcran1.push_back(Point3D(0,0,0)); listePlaceCadreEcran1.push_back(Point3D(0.45,0,0));
    Objet oCadreEcran1 = Objet(mPlastiqueEcran, Point3D(0.02,0.275,0.03), listePlaceCadreEcran1);
    listeHautEcran.push_back(oCadreEcran1);
    list<Point3D> listePlaceCadreEcran2; listePlaceCadreEcran2.push_back(Point3D(0.02,0,0)); listePlaceCadreEcran2.push_back(Point3D(0.02,0.255,0));
    Objet oCadreEcran2 = Objet(mPlastiqueEcran,Point3D(0.44,0.02,0.03),listePlaceCadreEcran2);
    listeHautEcran.push_back(oCadreEcran2);
    Objet oEcran = Objet(mEcran, Point3D(0.43,0.235,0.02), Point3D(0.02,0.02,0.01));
    listeHautEcran.push_back(oEcran);
    Objet oDerriereEcran = Objet(mPlastiqueEcran, Point3D(0.43,0.235,0.01), Point3D(0.02,0.02,0));
    listeHautEcran.push_back(oDerriereEcran);
    Objet oHautEcran = Objet(listeHautEcran,Point3D(1,1,1),Point3D(-0.145,0.08,0.095));
    listeMorceauEcran.push_back(oHautEcran);
    Objet oEcranPC = Objet(listeMorceauEcran,Point3D(1,1,1), Point3D(0,0,0));

    Objet oPC = Objet(mPC,Point3D(0.17,0.415,0.47),Point3D(0,0,0));
    list<Objet> listeTable;
    list<Point3D> listePlacePiedsTable; listePlacePiedsTable.push_back(Point3D(0,0,0)); listePlacePiedsTable.push_back(Point3D(0,0,0.75));
    listePlacePiedsTable.push_back(Point3D(1.55,0,0)); listePlacePiedsTable.push_back(Point3D(1.55,0,0.75));
    Objet oPiedsTable = Objet(mPiedsTable, Point3D(0.05,0.71,0.05), listePlacePiedsTable);
    listeTable.push_back(oPiedsTable);
    Objet oDessusTable = Objet(mBois, Point3D(1.6,0.02,0.8), Point3D(0,0.71,0));
    listeTable.push_back(oDessusTable);
    listeTable.push_back(Objet(oEcranPC, Point3D(1,1,1), Point3D(0.2,0.73,0.3) ));
    listeTable.push_back(Objet(oPC, Point3D(1,1,1), Point3D(1.2,0.73,0.3)));
    listeTable.push_back(Objet(oClavier, Point3D(1,1,1), Point3D(0.1,0.73,0.50)));
    listeTable.push_back(Objet(mSouris, Point3D(0.065,0.035,0.11), Point3D(0.8,0.73,0.4)));
    /* Chaise */
    list<Objet> listeChaise;
    Objet oPlatChaise = Objet(mChaise, Point3D(0.4,0.02,0.4),Point3D(0,0.38,0));
    listeChaise.push_back(oPlatChaise);
    list<Point3D> emplacementPiedChaise;
    emplacementPiedChaise.push_back(Point3D(0,0,0));
    emplacementPiedChaise.push_back(Point3D(0,0,0.38));
    emplacementPiedChaise.push_back(Point3D(0.38,0,0));
    emplacementPiedChaise.push_back(Point3D(0.38,0,0.38));
    emplacementPiedChaise.push_back(Point3D(0,0.4,0.38));emplacementPiedChaise.push_back(Point3D(0.38,0.4,0.38));
    Objet oPiedsChaise = Objet(mPiedsChaise, Point3D(0.02,0.38,0.02), emplacementPiedChaise);
    listeChaise.push_back(oPiedsChaise);
    Objet oHautChaise = Objet(mChaise, Point3D(0.40, 0.38, 0.02), Point3D(0.0,0.4,0.4));
    listeChaise.push_back(oHautChaise);
    Objet oChaise = Objet(listeChaise, Point3D(1,1,1), Point3D(0,0,0));
    list<Point3D> listeEmplacementChaise;
    listeEmplacementChaise.push_back(Point3D(0.2,0,0.6));
    listeEmplacementChaise.push_back(Point3D(1,0,0.6));
    listeTable.push_front(Objet(oChaise,Point3D(1,1,1), listeEmplacementChaise));

    Objet oTable = Objet(listeTable,Point3D(1,1,1), Point3D(0,0,0));
    list<Point3D> listeEmplacementsTable; listeEmplacementsTable.push_back(Point3D(0,0,0)); listeEmplacementsTable.push_back(Point3D(1.62,0,0));
    listeEmplacementsTable.push_back(Point3D(3.24,0,0));
    Objet oRangee = Objet(oTable,Point3D(1,1,1),listeEmplacementsTable);
    list<Point3D> listeEmplacementsRangee; listeEmplacementsRangee.push_back(Point3D(1.86,0,2.2)); listeEmplacementsRangee.push_back(Point3D(1.86,0,4)); listeEmplacementsRangee.push_back(Point3D(1.86,0,5.8)); listeEmplacementsRangee.push_back(Point3D(1.86,0,7.6));
    Objet oRangeeTable = Objet(oRangee, Point3D(1,1,1), listeEmplacementsRangee);
    scene.push_back(oRangeeTable);

    //Objet oMurGauche;
    list<Objet> listeMurGauche;
    Objet oMurHaut = Objet(mMur,Point3D(epaisseurMur,0.441+epaisseurMur,salleZ+2*epaisseurMur), Point3D(-epaisseurMur,2.289,-epaisseurMur));
    listeMurGauche.push_back(oMurHaut);
    Objet oMurBas = Objet(mMur, Point3D(epaisseurMur,0.6+epaisseurMur,salleZ+2*epaisseurMur), Point3D(-epaisseurMur,-epaisseurMur,-epaisseurMur));
    listeMurGauche.push_back(oMurBas);
    list<Point3D> listePlaceBlocFenetre; listePlaceBlocFenetre.push_back(Point3D(0,0.6,0.2)); listePlaceBlocFenetre.push_back(Point3D(0,0.6,2.35)); listePlaceBlocFenetre.push_back(Point3D(0,0.6,4.5)); listePlaceBlocFenetre.push_back(Point3D(0,0.6,7.28));
    Objet oFenetres = Objet(oBlocFenetre,Point3D(1,1,1),listePlaceBlocFenetre);
    listeMurGauche.push_back(oFenetres);
    list<Point3D> listeDebutFinMur; listeDebutFinMur.push_back(Point3D(-epaisseurMur,0.6,-epaisseurMur)); listeDebutFinMur.push_back(Point3D(-epaisseurMur,0.6,9.43));
    Objet oDebutFinMur = Objet(mMur, Point3D(epaisseurMur,1.689,0.2+epaisseurMur), listeDebutFinMur);
    listeMurGauche.push_back(oDebutFinMur);
    Objet oMilieuMur = Objet(mMur, Point3D(epaisseurMur,1.68,0.63), Point3D(-epaisseurMur,0.6,6.65));
    listeMurGauche.push_back(oMilieuMur);
    Objet oMurGauche = Objet(listeMurGauche,Point3D(1,1,1),Point3D(0,0,0));


    Objet oExterieur1 = Objet(mExterieur1, Point3D(2*salleX,2*salleY,0.05), Point3D(-2*salleX-epaisseurMur,-salleY/2,0));
    scene.push_back(oExterieur1);
    Objet oExterieur2 = Objet(mExterieur2, Point3D(0.05,2*salleY,2*salleX), Point3D(-2*salleX-epaisseurMur,-salleY/2,0));
    scene.push_back(oExterieur2);
    Objet oExterieur3 = Objet(mExterieur3, Point3D(2*salleX,2*salleY,0.05), Point3D(-2*salleX-epaisseurMur,-salleY/2,salleZ));
    scene.push_back(oExterieur3);

    scene.push_back(oMurGauche);


}
void affichage(){
    GLfloat Ox, Oy,Oz;

    GLfloat Ix = 0., Iy=1., Iz=2.;

    GLfloat pos_lum0[]={2.0,2.73,4.0,1.};
    GLfloat pos_lum1[] = {-6.7,1.3,4.3,0};
    GLfloat pos_lum2[] = {5,2.73,4.0,1};
    GLfloat pos_lum3[] = {2,2.73,8.0,1};
    GLfloat pos_lum4[] = {5,2.73,8.0,1};
    GLfloat dir_lum0[] = {0,-1,0};

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    rayon = distanceUser*cos(tetha);
    Ox = rayon * cos(angle);
    Oz = rayon * sin(angle);
    Oy = Cy + distanceUser * sin(tetha);
    cout << "Ox : " << Ox << "Oy : " << Oy << "Oz : " << Oz << "\n";
    //gluLookAt(2,2.2,1,0,0,0,1,0,1);

    gluLookAt(Ox,Oy,Oz, Cx, Cy,Cz, Ix,Iy,Iz);

    //glLightfv(GL_LIGHT1,GL_DIFFUSE, {0.98,0.98,0.98,1.});

    //glLightfv(GL_LIGHT0,GL_POSITION,pos_lum0);
    glLightfv(GL_LIGHT0, GL_POSITION, pos_lum0);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir_lum0);
    glLightfv(GL_LIGHT1, GL_POSITION, pos_lum1);
    glLightfv(GL_LIGHT2, GL_POSITION, pos_lum2);
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, dir_lum0);
    glLightfv(GL_LIGHT3, GL_POSITION, pos_lum3);
    glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, dir_lum0);
    glLightfv(GL_LIGHT4, GL_POSITION, pos_lum4);
    glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, dir_lum0);

    list<Objet>::iterator it = scene.begin();
    for(it;it != scene.end(); ++it) {
        glPushMatrix();
        glCallList(it->getObjet());
        glPopMatrix();
    }
    //glutWireCube(1);

    glutSwapBuffers();
}

void la_vue(int l, int h)
{
    GLfloat gauche=-.25,droite=.25;                      /* plans lateraux     */
    GLfloat bas=-.15625,haut=.15625;                     /* plans horizontaux  */
    GLfloat devant=0.5,derriere=25.;                     /* plans de facade    */

    if(h>l) h=l/1.6; else l=1.6*h;                       /* on conserve les proportions lar./hau. */

    glViewport(0.,0.,(GLsizei) l,(GLsizei) h);          /* definition de la fenetre logique      */

    glMatrixMode(GL_PROJECTION);                        /* activation de la matrice de projection */
    glLoadIdentity();
    glFrustum(gauche,droite,bas,haut,devant,derriere);  /* definition du volume de vue           */
}

void animer(int id){
    int err = glGetError();
    if(err != 0){
        cout << "Erreur : "<< gluErrorString(err) << err <<"\n";
    }
    //qangle +=0.01;
    glutPostRedisplay();
    glutTimerFunc(1000/30,&animer,id);
}

void click_souris(int bouton,int etat,int x,int y)
{
    if(etat==GLUT_DOWN){
        ancien_x=x;
        ancien_y=y;
        switch(bouton)
        {
            case GLUT_LEFT_BUTTON :
                mouv=GAUCHE;
                break;
            case GLUT_MIDDLE_BUTTON :
                mouv=MILIEU;
                break;
            case GLUT_RIGHT_BUTTON :
                mouv=DROIT;
                break;
            default :
                break;
        }
    }
    else if(etat==GLUT_UP) mouv=RIEN;
}

void souris(int x,int y)
{int dx,dy;

    dx=x-ancien_x;
    dy=y-ancien_y;
    double dAngleX = distanceUser * (-sin(angle) * cos(tetha));
    double dAngleY = distanceUser * (cos(angle) * cos(tetha));
    double lAngle = sqrt(pow(dAngleX,2)+pow(dAngleY,2));
    dAngleX /= lAngle;
    dAngleY /= lAngle;
    double dTethaX = distanceUser * (-sin(tetha) * cos(angle));
    double dTethaY = distanceUser * (-sin(tetha) * sin(angle));
    double dTethaZ = distanceUser * cos(tetha);
    double lTetha = sqrt(pow(dTethaX,2)+pow(dTethaY,2)+pow(dTethaZ,2));
    dTethaX /= lTetha;
    dTethaY /= lTetha;
    dTethaZ /= lTetha;

    switch(mouv){
        case GAUCHE :                                                /* depl. du global autour de la vue globale */
            if(dx<0) angle=angle-D_ANGLE;
            else if(dx>0) angle=angle+D_ANGLE;
            if(dy>0 && tetha<=M_PI/2.1) tetha=tetha+D_ANGLE;
            else if(dy<0 && tetha>=-M_PI/2.1) tetha=tetha-D_ANGLE;
            break;
        case DROIT :
            if(dx < 0) {
                Cx -= 0.1*dAngleX;
                Cy -= 0.1*dAngleY;
            }else if(dx > 0){
                Cx += 0.1*dAngleX;
                Cy += 0.1*dAngleY;
            }
            if(dy < 0){
                Cx -= 0.1 *dTethaX;
                Cy -= 0.1 *dTethaY;
                Cz -= 0.1 *dTethaZ;
            }else if(dy > 0){
                Cx += 0.1 *dTethaX;
                Cy += 0.1 *dTethaY;
                Cz += 0.1 *dTethaZ;
            }
            break;
        case MILIEU :                                                /* deplacement du global en avant ou en arriere */
            if (dy<0 && distanceUser>2.) distanceUser=distanceUser-0.1;
            else if (dy>0 && distanceUser<20.) distanceUser=distanceUser+0.1;
            break;
    }
    ancien_x=x;
    ancien_y=y;
    //cout << "Cx : " << Cx << "Cy : " << Cy << "Cz : " << Cz << "\n";
    glutPostRedisplay();
}

void initialisationLmieres(){
    GLfloat ambient[] = {1,1,1,1.};			/* comp. ambiante : lumiere multidirectionnelle  */
    GLfloat diffuse[] = {0.98,0.98,0.98,1.};     /* comp. diffuse  : lumiere unidirect. reflechie ds ttes les dir.       */
    GLfloat specular_reflexion[] = {0.8,0.8,0.8,1.};    /* comp. speculaire : lumiere unidirect. reflechie ds une dir. (=eclat) */
    GLfloat shiny_obj = 10.;
    float angle = 60;


    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,specular_reflexion);
    //glLightf(GL_LIGHT0,GL_CONSTANT_ATTENUATION,2.);      /* Kc coef. cst, independant de la distance obj./lum.                     */
    //glLightf(GL_LIGHT0,GL_LINEAR_ATTENUATION,1.);        /* Kl agit de facon lineaire par rapport a la distance obj./lum.          */
    glLightf(GL_LIGHT0,GL_QUADRATIC_ATTENUATION,0.01);    /* Kq agit sur le carre de la distance obj./lum.                          */
    glLighti(GL_LIGHT0, GL_SPOT_CUTOFF, angle);

    //soleil derriere la vitre
    glLightfv(GL_LIGHT1,GL_AMBIENT,ambient);				//les différents paramètres
    glLightfv(GL_LIGHT1,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT1,GL_SPECULAR,specular_reflexion);


    glLightfv(GL_LIGHT2,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT2,GL_SPECULAR,specular_reflexion);
    //glLightf(GL_LIGHT2,GL_CONSTANT_ATTENUATION,2.);      /* Kc coef. cst, independant de la distance obj./lum.                     */
    //glLightf(GL_LIGHT2,GL_LINEAR_ATTENUATION,1.);        /* Kl agit de facon lineaire par rapport a la distance obj./lum.          */
    glLightf(GL_LIGHT2,GL_QUADRATIC_ATTENUATION,0.01);    /* Kq agit sur le carre de la distance obj./lum.                          */
    glLighti(GL_LIGHT0, GL_SPOT_CUTOFF, angle);


    glLightfv(GL_LIGHT3,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT3,GL_SPECULAR,specular_reflexion);
    //glLightf(GL_LIGHT3,GL_CONSTANT_ATTENUATION,2.);      /* Kc coef. cst, independant de la distance obj./lum.                     */
    //glLightf(GL_LIGHT3,GL_LINEAR_ATTENUATION,1.);        /* Kl agit de facon lineaire par rapport a la distance obj./lum.          */
    glLightf(GL_LIGHT3,GL_QUADRATIC_ATTENUATION,0.01);    /* Kq agit sur le carre de la distance obj./lum.                          */
    glLighti(GL_LIGHT0, GL_SPOT_CUTOFF, angle);


    glLightfv(GL_LIGHT4,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT4,GL_SPECULAR,specular_reflexion);
    //glLightf(GL_LIGHT4,GL_CONSTANT_ATTENUATION,2.);      /* Kc coef. cst, independant de la distance obj./lum.                     */
    //glLightf(GL_LIGHT4,GL_LINEAR_ATTENUATION,1.);        /* Kl agit de facon lineaire par rapport a la distance obj./lum.          */
    glLightf(GL_LIGHT4,GL_QUADRATIC_ATTENUATION,0.01);    /* Kq agit sur le carre de la distance obj./lum.                          */
    glLighti(GL_LIGHT0, GL_SPOT_CUTOFF, angle);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);
    glEnable(GL_LIGHT4);

    glEnable(GL_COLOR_MATERIAL);							//spécification de la réflexion sur les matériaux
    glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
}

int main(int argc, char *argv[]){
    system("clear");

    /*list<Point3D> c;
    c.push_back(Point3D(0,5,3));*/
    glutInit(&argc,argv);

    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH|GLUT_ALPHA);
    //glShadeModel(GL_SMOOTH);

    glutInitWindowSize(1200,750);
    glutInitWindowPosition(10,10);
    glutCreateWindow("Tutu");

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);
    initialisationLmieres();
    glEnable(GL_LIGHTING);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
    glEnable(GL_NORMALIZE);
    //glEnable(GL_RESCALE_NORMAL);
    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);

    initObjet();

    glutDisplayFunc(&affichage);

    glutMotionFunc(&souris);
    glutMouseFunc(&click_souris);

    glutReshapeFunc(&la_vue);

    glutTimerFunc(1000/30,&animer,1);

    glutMainLoop();
}
