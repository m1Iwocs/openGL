#ifndef INFOGRAPHIECLASSE_OBJET_H
#define INFOGRAPHIECLASSE_OBJET_H


#include "Point3D.h"
#include "Materiaux.h"
#include <iostream>
#include <list>

using namespace std;

class Objet {
public:
    Objet(const Objet& autre);
    Objet(const Materiaux &a, Point3D taille, Point3D position);
    Objet(const Materiaux &a, Point3D taille, list<Point3D> position );
    Objet(const Materiaux &a, Point3D taille, Point3D position, bool sauvegarde);
    Objet(Objet a, Point3D taille, list<Point3D> position);
    Objet(Objet a, Point3D taille, Point3D position);
    Objet(list<Objet> a, Point3D taille, Point3D position);
    GLuint getObjet();
    void addRotation(int a);
private:
    list<Point3D> emplacements;
    Point3D taille;
    Materiaux* materiaux = nullptr;
    bool sauvegarde;
    GLuint objet;
    int angle = 0;
    list<Objet> composants;
    void init();
    bool test();
    void execute();

};


#endif //INFOGRAPHIECLASSE_OBJET_H
