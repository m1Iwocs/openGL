//
// Created by restil on 23/11/17.
//

#include "Point3D.h"

void Point3D::set(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;

}

Point3D::Point3D(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

Point3D::Point3D() {
    this->x = 0;
    this->y = 0;
    this->z = 0;

}
