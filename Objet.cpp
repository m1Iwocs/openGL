

#include "Objet.h"


Objet::Objet(const Objet &autre) {
    this->angle = autre.angle;
    this->emplacements = autre.emplacements;
    this->taille = autre.taille;
    if(autre.materiaux != nullptr)
        this->materiaux = new Materiaux(*autre.materiaux);
    this->sauvegarde = autre.sauvegarde;
    this->composants = autre.composants;
    this->angle = autre.angle;
}
Objet::Objet(const Materiaux &a, Point3D taille, Point3D position) {
    this->materiaux= new Materiaux(a);
    this->taille=taille;
    list<Point3D> b;
    b.push_back(position);
    this->sauvegarde=true;
    this->emplacements = b;
}

Objet::Objet(const Materiaux &a, Point3D taille, list<Point3D> position) {
    this->materiaux= new Materiaux(a);
    this->taille=taille;
    this->sauvegarde=true;
    this->emplacements = position;
}

Objet::Objet(const Materiaux &a, Point3D taille, Point3D position, bool sauvegarde) {
    this->materiaux= new Materiaux(a);
    this->taille=taille;
    list<Point3D> b;
    b.push_back(position);
    this->sauvegarde=sauvegarde;
    this->emplacements = b;
}

Objet::Objet(Objet a, Point3D taille, Point3D position) {
    this->taille = taille;
    list<Point3D> b;
    b.push_back(position);
    this->sauvegarde = true;
    this->emplacements = b;
    list<Objet> c;
    c.push_back(a);
    this->composants = c;
}

Objet::Objet(Objet a, Point3D taille, list<Point3D> position) {
    this->taille = taille;
    this->sauvegarde = true;
    this->emplacements = position;
    list<Objet> c;
    c.push_back(a);
    this->composants = c;
}

Objet::Objet(list<Objet> a, Point3D taille, Point3D position) {
    this->taille = taille;
    list<Point3D> b;
    b.push_back(position);
    this->sauvegarde = true;
    this->emplacements = b;
    list<Objet> d = list<Objet>(a);
    this->composants = d;
}


GLuint Objet::getObjet() {
    //cout << "1d\n";
    if(this->objet== 0){
        cout << "1.5d\n";
        this->init();cout << "2d\n";}
    return this->objet;
}

void Objet::init() {
    /* Initialisation des gluInt */
    if(this->materiaux != nullptr){
        this->materiaux->getObjet();
    }else {
        for(list<Objet>::iterator itComposants = composants.begin(); itComposants != composants.end(); ++itComposants){
            itComposants->getObjet();
        }
    }

    this->objet = glGenLists(1);
    cout << "ad\n";
    cout << "A0 : " << glGetError() << "\n";
    glNewList(this->objet, GL_COMPILE);
        if(this->materiaux != nullptr){
            cout << taille.x << "ac\n";
            //glScalef(taille.x,taille.y,taille.z);
            list<Point3D>::iterator it = emplacements.begin();
            for(it;it != emplacements.end(); ++it) {
                cout << "etr" << "x : " <<it->x<< " y : " << it->y << " z : " << it->z <<"etr\n";

                cout << "A1 : " << glGetError() << "\n";
                glPushMatrix();
                cout << "A2 : " << glGetError() << "\n";
                glTranslatef(it->x, it->y, it->z);
                cout << "A3 : " << glGetError() << "\n";
                glScalef(taille.x, taille.y, taille.z);
                cout << "A4 : " << glGetError() << "\n";
                glCallList(this->materiaux->getObjet());
                cout << "A5 : " << glGetError() << "\n";
                glPopMatrix();
                cout << "A6 : " << glGetError() << "\n";

            }
        }else{
            glPushMatrix();
            cout << "ab\n";


            for(list<Point3D>::iterator itEmplacements = emplacements.begin(); itEmplacements != emplacements.end(); ++itEmplacements){
                for(list<Objet>::iterator itComposants = composants.begin(); itComposants != composants.end(); ++itComposants){
                    glPushMatrix();
                        glTranslatef(itEmplacements->x,itEmplacements->y, itEmplacements->z);
                        glScalef(taille.x,taille.y,taille.z);
                        glRotatef(-90*angle,0,1,0);

                        if(itComposants->test())
                            glCallList(itComposants->getObjet());
                        else
                            itComposants->execute();
                    glPopMatrix();
                }
            }
            glPopMatrix();
        }
    cout << "A7 : " << glGetError() << "\n";
    glEndList();
    cout << "A8 : " << glGetError() << "\n";
    //glEnd();
    cout << "A9 : " << glGetError() << "\n";
}

bool Objet::test() {
    return this->sauvegarde;

}

void Objet::execute() {
    glPushMatrix();
    //glScalef(taille.x,taille.y,taille.z);
    list<Point3D>::iterator it = emplacements.begin();
    glTranslatef(it->x,it->y,it->z);
    glScalef(taille.x,taille.y,taille.z);
    glCallList(this->materiaux->getObjet());
    glPopMatrix();

}

void Objet::addRotation(int a) {
    this->angle = a;
}







