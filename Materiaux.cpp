#include "Materiaux.h"
#include <iostream>
#include <GL/glut.h>
#include "tga.h"
#include <GL/glu.h>
#include <memory.h>

using namespace std;

struct point{double x,y,z;};         /* definition d'un point */
typedef struct point point;

point sommet[6][4]={{{1,0,0}, {0,0,0},{0,1,0},{1,1,0}},   /* face 0 */
                    {{0,0,1},{1,0,1},{1,1,1},{0,1,1}},   /* face 1 */
                    {{0,0,0},{0,0,1},{0,1,1},{0,1,0}},   /* face 2 */
                    {{1,0,1}, {1,0,0},{1,1,0},{1,1,1}},   /* face 3 */
                    {{0,0,0},{1,0,0},{1,0,1},{0,0,1}},   /* face 4 */
                    {{0,1,1},{1,1,1},{1,1,0},{0,1,0}}};  /* face 5 */
//point normale[6]={{1.,0.,0.},{0.,0.,-1.},{-1.,0.,0.},{0.,0.,1.},{0.,1.,0.},{0.,-1.,0.}};
point normale[6] = {{0,0,-1}, {0,0,1}, {-1,0,0}, {1,0,0}, {0,-1,0}, {0,1,0}};

void charger_image(GLuint* id, const char* fichier)
{
    glGenTextures(1,id);
    glBindTexture(GL_TEXTURE_2D,*id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    gl_texture_t* tex = ReadTGAFile(fichier);
    glTexImage2D(GL_TEXTURE_2D,0,tex->internalFormat,tex->width,tex->height,0,tex->format,GL_UNSIGNED_BYTE,tex->texels);
    free(tex->texels);
    free(tex);
    glBindTexture(GL_TEXTURE_2D,0);
}

Image::Image(char* adresseImage) {
    adresse = strdup(adresseImage);

}
GLuint Image::getImage() {
    if(a == false)
        charger_image(&this->image,adresse);
    a = true;
    return this->image;
}

Image::Image(Image const& autre) {
    this->adresse = strdup(autre.adresse);
}

Couleur::Couleur(){
    this->r = 1.;
    this->b = 1.;
    this->g = 1.;
}

Couleur::Couleur(Couleur const& autre){
    cout << autre.r << " " << autre.g << " " << autre.b << " " << autre.a << "\n";
    this->a = autre.a;
    this->r = autre.r;
    this->g = autre.g;
    this->b = autre.b;
    cout << r << " " << g << " " << b << " " << a << "\n";
}

Couleur::Couleur(float r, float g, float b) {
    this->r=r;
    this->b=b;
    this->g=g;
}
Couleur::Couleur(float r, float g, float b, float a) {
    this->a=a;
    this->b=b;
    this->g=g;
    this->r=r;
    cout << r << " " << g << " " << b << " " << a << "\n";
}


Texture::Texture() {
    this->couleur = nullptr;
    this->image = nullptr;
}

Texture::Texture(Texture const& autre) {
    cout << "A\n";
    if(autre.couleur != nullptr)
        this->couleur = new Couleur(*autre.couleur);
    if(autre.image != nullptr)
        this->image = new Image(*autre.image);
    cout << "B\n";
}
Texture::Texture(const Couleur &couleur) {
    //this->couleur = new Couleur(couleur.r,couleur.g,couleur.b,couleur.a);
    this->couleur = new Couleur(couleur);
}
Texture::Texture(char *image) {
    this->image = new Image(image);
}

Texture::Texture(const Couleur &couleur, char *image) {
    this->couleur = new Couleur(couleur);
    this->image = new Image(image);
}

Texture::~Texture() {
    cout << "3\n";
    if(this->couleur != nullptr)
        delete couleur;
    /*if(this->image != nullptr)
        delete image;*/
}
bool Texture::isAll() {
    return (couleur== nullptr&&image == nullptr);
}
bool Texture::isCouleur() {
    return couleur != nullptr;
}
bool Texture::isTexture() {
    return image != nullptr;
}

/**
 *
 *      Materiaux
 *
 */

Materiaux::Materiaux() {
    for(int i = 0; i<6; ++i)
        this->textures[i] = Texture(Couleur(1,1,1));
}

Materiaux::Materiaux(Materiaux const &autre) {
    for(int i = 0; i<6; ++i)
        this->textures[i] = *(new Texture(autre.textures[i]));
}

Materiaux::Materiaux(const Couleur &couleur) {
    //Texture *a = new Texture(couleur);
    for(int i=0; i<6;++i)
        //this->textures[i] = *a;
    this->textures[i] = *(new Texture(couleur));
}
Materiaux::Materiaux(Texture texture) {
    Texture *a = new Texture(texture);
    for(int i=0;i<6;++i)
        this->textures[i] = *a;

}
Materiaux::Materiaux(Texture* tabTexture) {
    for(int i =0; i<6; ++i)
        this->textures[i] = *(new Texture(tabTexture[i]));
}
GLuint Materiaux::getObjet() {
    cout << "On creer Materiaux\n";
    if(this->objet== 0){
        cout << "C0\n";
        this->init();}
    cout << "Objet : " << this->objet << "\n";
    return this->objet;
}



void Materiaux::init() {
    cout << "B0 : " << glGetError() << "\n";
    this->objet = glGenLists(1);
    cout << "B1 : " << glGetError() << "\n";
    glNewList(this->objet, GL_COMPILE);
    glMaterialfv(GL_FRONT,GL_AMBIENT,this->mat_ambiante);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,this->mat_diffuse);
    glMaterialfv(GL_FRONT,GL_SPECULAR,this->mat_speculaire);
    glMaterialf(GL_FRONT,GL_SHININESS,0.);
    cout << "B2 : " << glGetError() << "\n";
    for(int i = 0; i< 6; ++i){
        cout << "B3(" << i << ") : " << glGetError() << "\n";
        if(this->textures[i].isTexture()){
            cout << "B5(" << i << ") : " << glGetError() << "\n";
            //glClear(GL_COLOR_BUFFER_BIT);
            cout << "B6(" << i << ") : " << glGetError() << "\n";
            if(this->textures[i].isCouleur())
                //glColor4f(this->textures[i].couleur->r)
                glColor4f(this->textures[i].couleur->r,this->textures[i].couleur->g,this->textures[i].couleur->b,this->textures[i].couleur->a);
            else
                glColor4f(1,1,1,1);
            cout << "B7(" << i << ") : " << glGetError() << "\n";
            glBindTexture(GL_TEXTURE_2D,this->textures[i].image->getImage());

            glBegin(GL_QUADS);

            glNormal3f(normale[i].x,normale[i].y,normale[i].z);
            glTexCoord2f(0.,0.);
            glVertex3f(sommet[i][0].x,sommet[i][0].y,sommet[i][0].z);
            glTexCoord2f(1.,0.);
            glVertex3f(sommet[i][1].x,sommet[i][1].y,sommet[i][1].z);
            glTexCoord2f(1.,1.);
            glVertex3f(sommet[i][2].x,sommet[i][2].y,sommet[i][2].z);
            glTexCoord2i(0.,1.);
            glVertex3f(sommet[i][3].x,sommet[i][3].y,sommet[i][3].z);

            glEnd();

            glBindTexture(GL_TEXTURE_2D,0);
        }else{
            cout << "B4(" << i << ") : " << glGetError() << "\n";
            glColor4f(this->textures[i].couleur->r,this->textures[i].couleur->g,this->textures[i].couleur->b,this->textures[i].couleur->a);
            cout << "BB12" << " R : " << this->textures[i].couleur->r << " G : " << this->textures[i].couleur->g << " B : " << this->textures[i].couleur->b << " A : " << this->textures[i].couleur->a << "\n";
            cout << "B8(" << i << ") : " << glGetError() << "\n";
            glBegin(GL_QUADS);
            cout << "B9(" << i << ") : " << glGetError() << "\n";

            glNormal3f(normale[i].x,normale[i].y,normale[i].z);
            //glTexCoord2f(0.,0.);
            glVertex3f(sommet[i][0].x,sommet[i][0].y,sommet[i][0].z);
            //glTexCoord2f(3.,0.);
            glVertex3f(sommet[i][1].x,sommet[i][1].y,sommet[i][1].z);
            //glTexCoord2f(3.,3.);
            glVertex3f(sommet[i][2].x,sommet[i][2].y,sommet[i][2].z);
            //glTexCoord2i(0.,3.);
            glVertex3f(sommet[i][3].x,sommet[i][3].y,sommet[i][3].z);

            cout << "B10(" << i << ") : " << glGetError() << "\n";
            glEnd();
            cout << "B11(" << i << ") : " << glGetError() << "\n";

        }
        cout << "B5 : " << glGetError() << "\n";
        //glClear(GL_COLOR_BUFFER_BIT);
        cout << "B6 : " << glGetError() << "\n";
    }

    cout << "B7 : " << glGetError() << "\n";
    glEndList();
    cout << "B8 : " << glGetError() << "\n";
}

Materiaux::~Materiaux() {
    //delete objet;
    cout << "T1\n";
    //delete[] mat_ambiante;
    cout << "T2\n";
    //delete mat_diffuse;
    cout << "T3\n";
    //delete mat_speculaire;
    cout << "T4\n";
    /*for(int i =0; i<6; ++i)
        delete &textures[i];*/
    cout << "T5\n";
    //delete[] textures;
    cout << "T6\n";
    //delete textures;

}

void Materiaux::setSpeculaire(Couleur a) {
    this->mat_speculaire[0] = a.r;
    this->mat_speculaire[1] = a.g;
    this->mat_speculaire[2] = a.b;
    this->mat_speculaire[3] = a.a;
}
